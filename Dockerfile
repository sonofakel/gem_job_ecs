FROM python:3.8-slim

WORKDIR /gem_job
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY app.py .

CMD ["python3", "/gem-job/app.py"]
